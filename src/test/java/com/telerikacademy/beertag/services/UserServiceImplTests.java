package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.*;
import com.telerikacademy.beertag.repositories.BeerRepositoryImpl;
import com.telerikacademy.beertag.repositories.UserRepositoryImpl;
import com.telerikacademy.beertag.repositories.contracts.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests<UserRepositoryImpl> {
    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    private BeerRepositoryImpl mockBeerRepository;

    Country mockCountry = new Country("Spain");
    String authority = "ROLE_ADMIN";
    Style mockStyle = new Style("ale");
    Brewery mockBrewery = new Brewery("Corona",mockCountry);

    User mockUser = new User("gosho", "123456", "gosho@abv.bg");
    Beer mockBeer = new Beer("Kamenitza", mockBrewery, 2.5, "Amazing beer",
            mockStyle, 3.4);


    @Test
    public void get_All_Users_Should_Return_A_List_Of_Users() {

        //Arrange
        Mockito.when(mockUserRepository.getAllUsers())
                .thenReturn(Arrays.asList(
                        new User("gosho", "123456", "gosho@abv.bg")));
        //Act
        List<User> users = userService.getAll();

        //Assert
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void get_UserById_Should_Return_A_User_When_Id_Matches() {
        Mockito.when(mockUserRepository.getUserById(1))
                .thenReturn(new User("gosho", "123456", "gosho@abv.bg"));
        ;

        User user = userService.getById(1);

        Assert.assertEquals("gosho", user.getUsername());
    }


    @Test
    public void getUserByName_Should_Return_A_User_When_Name_Matches() {
        Mockito.when(mockUserRepository.getUserByUserName("gosho"))
                .thenReturn(new User("gosho", "123456", "gosho@abv.bg"));

        User user = userService.getUserByUserName("gosho");

        Assert.assertEquals("gosho", user.getUsername());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void getUserByName_Should_Return_An_Exception_When_Name_Doesnt_Match() {
        User user = userService.getUserByUserName("name");
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        User user = new User("gosho", "123456", "gosho@abv.bg");
        userService.createUser(user, authority);
        Mockito.verify(mockUserRepository, Mockito.times(1)).createUser(user, authority);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        User user = new User("gosho", "123456", "gosho@abv.bg");
        userService.update(1,user);
        Mockito.verify(mockUserRepository, Mockito.times(1)).updateUser(user);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        userService.delete(2);
        Mockito.verify(mockUserRepository, Mockito.times(1)).delete(2);
    }

    @Test
    public void addToWishList_Should_Call_The_User_repository(){
        userService.addBeerToWishList(1, 1);

        Mockito.verify(mockUserRepository, Mockito.times(1)).addToWishList(1, 1);
    }

    @Test
    public void addToDrankList_Should_Call_The_User_repository(){
        userService.addBeerToDrunkList(1, 1);

        Mockito.verify(mockUserRepository, Mockito.times(1)).addToDrinkList(1, 1);
    }

}
