package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repositories.contracts.StyleRepository;
import com.telerikacademy.beertag.repositories.contracts.TagRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {
    @Mock
    TagRepository mockTagRepository;

    @InjectMocks
    TagServiceImpl tagService;

   Tag mockTag = new Tag("awesome");

    @Test
    public void get_All_Should_Return_A_List_Of_Tags() {
        Mockito.when(mockTagRepository.getAllTags())
                .thenReturn(Arrays.asList(mockTag));

        List<Tag> tags = tagService.getAll();
        Assert.assertEquals(1, tags.size());
    }

    @Test
    public void get_Tag_By_ID_Should_Return_A_Tag() {
        Mockito.when(mockTagRepository.getTadById(1))
                .thenReturn(new Tag("awesome"));

        Tag tag = tagService.getById(1);

        Assert.assertEquals("awesome", tag.getName());

    }

    @Test
    public void get_Tag_By_Name_Should_Return_A_Country() {
        Mockito.when(mockTagRepository.getTagByName("awesome"))
                .thenReturn(new Tag("awesome"));

        Tag tag = tagService.getTagByName("awesome");

        Assert.assertEquals("awesome", tag.getName());

    }

    @Test
    public void create_Should_Invoke_Repository_Create() {
        Tag tag = new Tag("awesome");
        tagService.create(tag);
        Mockito.verify(mockTagRepository, Mockito.times(1)).createTag(tag);
    }
    @Test
    public void update_Should_Invoke_Repository_Update() {
        Tag tag = new Tag("awesome");
        tagService.update(1,tag);
        Mockito.verify(mockTagRepository, Mockito.times(1)).updateTag(tag,1);
    }

    @Test
    public void delete_Should_Invoke_Repository_Delete() {
       tagService.delete(1);
        Mockito.verify(mockTagRepository, Mockito.times(1)).deleteTag(1);
    }
}
