package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.exceptions.ObjectAlreadyExistsException;
import com.telerikacademy.beertag.models.*;
import com.telerikacademy.beertag.repositories.contracts.BeerRepository;
import com.telerikacademy.beertag.repositories.contracts.CountryRepository;
import com.telerikacademy.beertag.repositories.contracts.StyleRepository;
import com.telerikacademy.beertag.repositories.contracts.TagRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {
    @Mock
    BeerRepository mockBeerRepository;

    @Mock
    StyleRepository mockStyleRepository;

    @Mock
    TagRepository mockTagRepository;

    @Mock
    CountryRepository mockCountryRepository;

    @InjectMocks
    BeerServiceImpl beerService;


    Style mockStyle = new Style("Ale");
    Country mockCountry = new Country("Spain");
    Brewery mockBrewery = new Brewery("Kamenitza", mockCountry);
    Tag mockTag = new Tag("#awesome");


    @Test
    public void get_All_Should_Return_A_List_Of_Beers() {

        //Arrange
        Mockito.when(mockBeerRepository.getAllBeers())
                .thenReturn(Arrays.asList(
                        new Beer("Kamenitza", mockBrewery, 2.5, "Amazing beer",
                                mockStyle, 3.4)));
        //Act
        List<Beer> beers = beerService.getAll();

        //Assert
        Assert.assertEquals(1, beers.size());
    }

    @Test
    public void get_Beer_By_Id_Should_Return_A_Beer_When_Id_Matches() {
        Mockito.when(mockBeerRepository.getBeerById(1))
                .thenReturn(new Beer("Kamenitza", mockBrewery, 2.5, "Amazing beer",
                        mockStyle, 3.4));

        Beer beer = beerService.getById(1);

        Assert.assertEquals("Kamenitza", beer.getBeerName());
    }


    @Test(expected = ObjectAlreadyExistsException.class)
    public void create_Beer_Should_Return_An_Exception_When_Beer_Exists() {
        //Arrange
        Mockito.when(mockBeerRepository.getAllBeers())
                .thenReturn(Arrays.asList(new Beer("Zagorka", mockBrewery, 2.55, "Amazing beer",
                        mockStyle, 5)));

        //Act
        beerService.create(new Beer("Zagorka", mockBrewery, 2.55, "Amazing beer",
                mockStyle, 5));

        Mockito.verify(mockBeerRepository, Mockito.times(0))
                .addNewBeer(new Beer("Zagorka", mockBrewery, 2.55, "Amazing beer",
                        mockStyle, 5));
    }

    @Test
    public void create_Should_Invoke_Repository_Create() {
        Beer beer = new Beer("Zagorka", mockBrewery, 2.55, "Amazing beer",
                mockStyle, 5);
        beerService.create(beer);
        Mockito.verify(mockBeerRepository, Mockito.times(1)).addNewBeer(beer);
    }
    @Test
    public void update_Should_Invoke_Repository_Update() {
        Beer beer = new Beer("Zagorka", mockBrewery, 2.55, "Amazing beer",
                mockStyle, 5);
        beerService.update(1, beer);
        Mockito.verify(mockBeerRepository, Mockito.times(1)).updateBeer(1, beer);
    }

    @Test
    public void delete_Should_Invoke_Repository_Delete() {
        beerService.delete(1);
        Mockito.verify(mockBeerRepository, Mockito.times(1)).deleteBeer(1);
    }

    @Test
    public void getBeerByName_Should_Return_A_Beer_When_Name_Matches() {
        Mockito.when(mockBeerRepository.getBeerByName("Corona"))
                .thenReturn(new Beer("Corona", mockBrewery, 2.5, "Amazing beer",
                        mockStyle, 3.4));

        Beer beer = beerService.getBeerByName("Corona");

        Assert.assertEquals("Corona", beer.getBeerName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getBeerByName_Should_Throw_An_Exception_When_Name_Does_Not_Match() {
        Beer beer = beerService.getBeerByName("Kom");

    }

}