package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.repositories.contracts.CountryRepository;
import com.telerikacademy.beertag.services.contracts.CountryService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository mockCountryRepository;

    @InjectMocks
    CountryServiceImpl countryService;

    Country mockCountry = new Country("Spain");

    @Test
    public void get_All_Should_Return_A_List_Of_Countries(){
        Mockito.when(mockCountryRepository.getAllCountries())
                .thenReturn(Arrays.asList(mockCountry));

        List<Country> countries = countryService.getAll();
        Assert.assertEquals(1,countries.size());
    }

    @Test
    public void get_Country_By_ID_Should_Return_A_Country() {
        Mockito.when(mockCountryRepository.getCountryById(1))
                .thenReturn(new Country("Spain"));

        Country country = countryService.getCountryById(1);

        Assert.assertEquals("Spain", country.getOriginCountry());

    }

    @Test
    public void get_Country_By_Name_Should_Return_A_Country() {
        Mockito.when(mockCountryRepository.getCountryByName("Spain"))
                .thenReturn(new Country("Spain"));

        Country country = countryService.getCountryByName("Spain");

        Assert.assertEquals("Spain", country.getOriginCountry());

    }


}
