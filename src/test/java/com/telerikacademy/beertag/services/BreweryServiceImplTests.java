package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.exceptions.ObjectAlreadyExistsException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.repositories.contracts.BreweryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
@RunWith(MockitoJUnitRunner.class)
public class BreweryServiceImplTests {

    @Mock
    BreweryRepository mockBreweryRepository;

    @InjectMocks
    BreweryServiceImpl breweryService;
    Country mockCountry = new Country("Spain");
    Brewery mockBrewery = new Brewery("Corona",mockCountry);

    @Test
    public void get_All_Should_Return_A_List_Of_Breweries() {
        //Arrange
        Mockito.when(mockBreweryRepository.getAllBreweries())
                .thenReturn(Arrays.asList(mockBrewery));
        //Act
        List<Brewery> breweries = breweryService.getAll();
        //Assert
        Assert.assertEquals(1, breweries.size());
    }

    @Test
    public void get_Beer_By_Id_Should_Return_A_Beer_When_Id_Matches() {
        Mockito.when(mockBreweryRepository.getBreweryById(1))
                .thenReturn(new Brewery("Corona",mockCountry));

        Brewery brewery = breweryService.getBreweryById(1);

        Assert.assertEquals("Corona", brewery.getName());
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        Brewery brewery = new Brewery();
        breweryService.create(brewery);
        Mockito.verify(mockBreweryRepository, Mockito.times(1)).createBrewery(brewery);
    }

    @Test
    public void update_Should_Invoke_Repository_Update() {
        Brewery brewery = new Brewery();
        breweryService.update(1, brewery);
        Mockito.verify(mockBreweryRepository, Mockito.times(1)).updateBrewery(1, brewery);
    }

    @Test
    public void delete_Should_Invoke_Repository_Delete() {
        breweryService.delete(1);
        Mockito.verify(mockBreweryRepository, Mockito.times(1)).deleteBrewery(1);
    }

    @Test
    public void getBreweryByName_Should_Return_A_Brewery_When_Name_Matches() {
        Mockito.when(mockBreweryRepository.getByName("Corona"))
                .thenReturn(new Brewery("Corona",mockCountry));

        Brewery brewery= breweryService.getByName("Corona");

        Assert.assertEquals("Corona", brewery.getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getBeerByName_Should_Throw_An_Exception_When_Name_Does_Not_Match() {
       Brewery brewery = breweryService.getByName("Kom");

    }
}
