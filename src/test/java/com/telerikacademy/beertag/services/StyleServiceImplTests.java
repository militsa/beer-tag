package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.repositories.contracts.CountryRepository;
import com.telerikacademy.beertag.repositories.contracts.StyleRepository;
import com.telerikacademy.beertag.services.contracts.StyleService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.swing.text.StyledEditorKit;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {


    @Mock
    StyleRepository mockStyleRepository;

    @InjectMocks
    StyleServiceImpl styleService;

    Style mockStyle = new Style();

    @Test
    public void get_All_Should_Return_A_List_Of_Style() {
        Mockito.when(mockStyleRepository.getAll())
                .thenReturn(Arrays.asList(mockStyle));

        List<Style> styles = styleService.getAll();
        Assert.assertEquals(1, styles.size());
    }

    @Test
    public void get_Style_By_ID_Should_Return_A_Style() {
        Mockito.when(mockStyleRepository.getStyleById(1))
                .thenReturn(new Style("ale"));

        Style style = styleService.getStyleById(1);

        Assert.assertEquals("ale", style.getBeerStyle());

    }

    @Test
    public void get_Country_By_Name_Should_Return_A_Country() {
        Mockito.when(mockStyleRepository.getStyleByName("ale"))
                .thenReturn(new Style("ale"));

        Style style = styleService.getStyleByName("ale");

        Assert.assertEquals("ale", style.getBeerStyle());

    }

    @Test
    public void create_Should_Invoke_Repository_Create() {
        Style style = new Style("ale");
        styleService.create(style);
        Mockito.verify(mockStyleRepository, Mockito.times(1)).createStyle(style);
    }
    @Test
    public void update_Should_Invoke_Repository_Update() {
        Style style = new Style("IPA");
        styleService.update(1,style);
        Mockito.verify(mockStyleRepository, Mockito.times(1)).updateStyle(1,style);
    }

    @Test
    public void delete_Should_Invoke_Repository_Delete() {
        styleService.delete(1);
        Mockito.verify(mockStyleRepository, Mockito.times(1)).deleteStyle(1);
    }

}
