package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.models.*;
import com.telerikacademy.beertag.repositories.contracts.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceImplTests {
    @Mock
    RatingRepository mockRatingRepository;


    @InjectMocks
    RatingServiceImpl ratingService;

    Style mockStyle = new Style("Ale");
    Country mockCountry = new Country("Spain");
    Brewery mockBrewery = new Brewery("Kamenitza", mockCountry);
    Tag mockTag = new Tag("#awesome");
    Beer mockBeer = new Beer("Zagorka", mockBrewery, 2.55, "Amazing beer",
            mockStyle, 5);
    User mockUser = new User();


    @Test
    public void create_Should_Invoke_Repository_Create() {
        ratingService.addRating(1,1,0);
        Mockito.verify(mockRatingRepository, Mockito.times(1)).addRatingToBeer(1,1,0);
    }
    @Test
    public void update_Should_Invoke_Repository_Update() {
        ratingService.update(1, mockBeer.getId(),mockUser.getId());
        Mockito.verify(mockRatingRepository, Mockito.times(1)).updateRating(1, mockUser.getId(),mockBeer.getId());
    }


}
