package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.repositories.contracts.StyleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Style getStyleById(int id){
        try (Session session = sessionFactory.openSession()){
            return session.get(Style.class,id);
        }
    }

    @Override
    public Style getStyleByName(String name){
        try (Session session = sessionFactory.openSession()){
            Query<Style> query = session.createQuery("from Style where beerStyle=:name");
            query.setParameter("name",name);
            return query.list().stream().findFirst().orElse(null);
        }
    }

    @Override
    public List<Style> getAll(){
        try (Session session = sessionFactory.openSession()){
            Query<Style> query = session.createQuery("from Style",Style.class);
            return query.list();
        }
    }

    @Override
    public void createStyle(Style style){
        try (Session session = sessionFactory.openSession()){
            session.save(style);
        }
    }

    @Override
    public Style updateStyle(int id, Style style){
        try (Session session = sessionFactory.openSession()) {
            try {
                Transaction txn = session.beginTransaction();
            Query query = session.createQuery("update Style set beerStyle =: style where id =:id");
            query.setParameter("id", id);
            query.setParameter("style", style.getBeerStyle());
            query.executeUpdate();
            return style;
            }catch (Exception e) {
                session.getTransaction().rollback();
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteStyle(int id){
        try (Session session = sessionFactory.openSession()){
            try {
                Transaction txn = session.beginTransaction();
                Query query = session.createQuery("delete Style where id=:id");
                query.setParameter("id", id);
                query.executeUpdate();
            }catch (Exception e) {
                session.getTransaction().rollback();
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }
}
