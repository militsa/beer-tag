package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.repositories.contracts.BreweryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Brewery getBreweryById(int id){
        try (Session session = sessionFactory.openSession()){
            return session.get(Brewery.class,id);
        }
    }
////TODO - ask Lacho whether I need the query if name is set as unique
//    public Brewery getByName(String breweryName){
//        try (Session session = sessionFactory.openSession()){
////            Query<Brewery> query = session.createQuery("from Brewery  where name=:name",Brewery.class);
////            query.setParameter("name",breweryName);
////            return query.list().get(0);
//            Brewery brewery = session.get(Brewery.class,breweryName);
//            return brewery;
//        }
//    }

    @Override
    public Brewery getByName(String name) {
        try (Session session =sessionFactory.openSession()){
            Query<Brewery> query =session.createQuery("from Brewery where name =:name",Brewery.class);
            query.setParameter("name",name);
            return query.list().stream().findFirst().orElse(null);
        }
    }

    public List<Brewery> getAllBreweries(){
        try( Session session = sessionFactory.openSession()){
            Query<Brewery> query = session.createQuery("from Brewery ",Brewery.class);
            return query.list();
        }
    }

    public void createBrewery(Brewery brewery){
        try (Session session = sessionFactory.openSession()){
            session.save(brewery);
        }
    }

    public Brewery updateBrewery(int id, Brewery brewery){
        try (Session session = sessionFactory.openSession()){
            try {
                Transaction txn = session.beginTransaction();
                Query query = session.createQuery("update Brewery set name=:name where id =:id");
                query.setParameter("id", id);
                query.setParameter("name", brewery.getName());
                query.executeUpdate();
                return brewery;
            }catch (Exception e) {
                session.getTransaction().rollback();
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteBrewery(int id){
        try (Session session = sessionFactory.openSession()){
            try {
                Transaction txn = session.beginTransaction();
            Query query = session.createQuery("delete Brewery where id =:id");
            query.setParameter("id",id);
            query.executeUpdate();
            }catch (Exception e) {
                session.getTransaction().rollback();
                e.printStackTrace();
                throw new RuntimeException(e);
            }

        }
    }


}
