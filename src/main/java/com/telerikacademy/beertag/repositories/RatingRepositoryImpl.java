package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.repositories.contracts.RatingRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class RatingRepositoryImpl implements RatingRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Rating getRating(int userid, int beerid) {
        try (Session session = sessionFactory.openSession()) {
            Query<Rating> query = session.createNativeQuery("select * from ratings " +
                    "where user_id=:userid and beer_id=:beerid", Rating.class)
                    .setParameter("userid", userid)
                    .setParameter("beerid", beerid);
            return query.list().stream().findFirst().orElse(null);
        }
    }

    public void updateRating(int rating, int userId, int beerId) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                User user = session.get(User.class, userId);
                Beer beer = session.get(Beer.class, beerId);
                Rating rating1 = beer.getRatings().stream().filter(r -> r.getUser().getId() == userId).findFirst().orElse(null);
                rating1.setBeerRating(rating);
                session.update(rating1);
                session.delete(rating1);
                session.getTransaction().commit();

            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }

    }

    @Override
    public void addRatingToBeer(int rating, int userId, int beerId) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                User user =session.get(User.class,userId);
                Beer beer =session.get(Beer.class,beerId);
                Rating ratingObj = new Rating(rating,beer,user);
                session.save(ratingObj);
                session.getTransaction().commit();
                }catch (HibernateException e){
                session.getTransaction().rollback();
            }


        }
    }


    @Override
    public List<Rating> allRatingObjects() {
        try (Session session=sessionFactory.openSession()){
            Query<Rating> q =session.createQuery("from Rating",Rating.class);
            return q.list();
        }


    }
}
