package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.repositories.contracts.CountryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Country getCountryById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Country.class, id);
        }
    }

    @Override
    public List<Country> getAllCountries() {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country", Country.class);
            return query.list();
        }
    }

    public Country getCountryByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> country = session.createQuery("from Country where originCountry=:name");
            country.setParameter("name",name);
            return country.list().stream().findFirst().orElse(null);
        }
    }


    // not needed methods below


//    public void createCountry(Country country){
//        try (Session session = sessionFactory.openSession()){
//            session.save(country);
//        }
//    }

//    public Country updateCountry(int id, Country country ){
//        try (Session session = sessionFactory.openSession()){
//            try {
//                Transaction txn = session.beginTransaction();
//            Query query = session.createQuery("update Country set originCountry=:name where countryId=:id");
//            query.setParameter("id",id);
//            query.setParameter("name", country.getOriginCountry());
//            query.executeUpdate();
//            return country;
//            }catch (Exception e) {
//                session.getTransaction().rollback();
//                e.printStackTrace();
//                throw new RuntimeException(e);
//            }
//        }
//    }
//
//    public void deleteCountry(int id){
//        try (Session session = sessionFactory.openSession()){
//            try {
//                Transaction txn = session.beginTransaction();
//                Query query = session.createQuery("delete Country where id =:id");
//                query.setParameter("id", id);
//                query.executeUpdate();
//            }catch (Exception e) {
//                session.getTransaction().rollback();
//                e.printStackTrace();
//                throw new RuntimeException(e);
//            }
//        }
//    }


}
