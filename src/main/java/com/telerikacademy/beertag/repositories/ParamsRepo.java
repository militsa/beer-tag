package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.Params;
import org.springframework.stereotype.Repository;
import java.util.Stack;

@Repository
public class ParamsRepo {
    public Stack<Params> stck;


    public ParamsRepo() {
        this.stck = new Stack<>();
    }

    public void createParam(Params params){
        stck.add(params);
    }

    public Params getParams(){
        return stck.pop();
    }

}
