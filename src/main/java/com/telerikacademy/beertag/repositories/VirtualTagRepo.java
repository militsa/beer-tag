package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.VirtualTag;
import org.springframework.stereotype.Repository;

import java.util.Stack;

@Repository
public class VirtualTagRepo {
    private Stack<VirtualTag> stack;

    public VirtualTagRepo() {
        stack = new Stack<VirtualTag>();
    }

    public void createVirtualTag(VirtualTag virtualTag){
        stack.add(virtualTag);
    }

    public VirtualTag getVirtualTag(){
        return stack.pop();
    }
}
