package com.telerikacademy.beertag.repositories;

import com.sun.jndi.ldap.Ber;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.repositories.contracts.UserRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;


import javax.jws.soap.SOAPBinding;
import javax.persistence.EntityNotFoundException;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public User getUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(User.class, id);
        }
    }


//    public List<Beer> getTop3MostRankedBeersByUser(int userid) {
//        try (Session session = sessionFactory.openSession()) {
//            Query<Beer> query = session.createSQLQuery("select b.* from beers b " +
//                    "join drank_beers db on b.beer_id = db.beer_id " +
//                    "join ratings r on b.beer_id = r.beer_id " +
//                    "where db.user_id=:userid order by r.rating desc");
//            query.setParameter("userid", userid);
//            query.setMaxResults(3);
//            return query.list();
//        }
//    }

    public User getUserByUserName(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query =
                    session.createQuery("from User where username=:username", User.class);
            query.setParameter("username", username);
           return query.uniqueResult();
        }
    }

    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return getUserByUserName(authentication.getName());
    }

    public void updateUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            User userToUpdate = getCurrentUser();
            if (!user.getPassword().equals(userToUpdate.getPassword())) {
                BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
                userToUpdate.setPassword(bCryptPasswordEncoder.encode((user.getPassword())));
            }
            session.update(userToUpdate);
        }
    }

    public void updateUserAdmin(int id, User user, String authority){
        try (Session session = sessionFactory.openSession()){

            try {

                session.getTransaction().begin();
                Query query = session.createSQLQuery("delete  from authorities " +
                        "where user_id=:id");
                query.setParameter("id", id);
                query.executeUpdate();

                query = session.createSQLQuery("insert  into authorities(user_id, role) VALUES (:userid,:authority)");
                query.setParameter("userid", user.getId());
                query.setParameter("authority", authority);
                query.executeUpdate();

                query = session.createSQLQuery("update authorities set role=:role where user_id=:id");
                query.setParameter("id", user.getId());
                query.setParameter("role", authority);
                        query.executeUpdate();


                query = session.createSQLQuery("update users set " +
                        "username=:username," +
                        "password=:password,  " +
                        "email=:email where user_id=:id");
                query.setParameter("password", user.getPassword());
                query.setParameter("username", user.getUsername());
                query.setParameter("email", user.getEmail());
                query.executeUpdate();

                session.getTransaction().commit();
            }catch (HibernateException e){
                session.getTransaction().rollback();
            }
        }
    }

    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            try {
             session.beginTransaction();
                Query query= session.createSQLQuery("delete from ratings where user_id=:id");
                query.setParameter("id", id);
                query.executeUpdate();

                query= session.createSQLQuery("delete from drank_beers where user_id=:id");
                query.setParameter("id", id);
                query.executeUpdate();

                query= session.createSQLQuery("delete from want_to_drink where user_id=:id");
                query.setParameter("id", id);
                query.executeUpdate();

                query = session.createSQLQuery("delete from authorities where user_id=:id");
                query.setParameter("id", id);
                query.executeUpdate();

                query = session.createSQLQuery("delete from users where user_id=:id");
                query.setParameter("id", id);

                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }

    public void createUser(User user, String authority) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(user);
            Query query = session.createSQLQuery("insert into authorities(user_id, role) VALUES (:userid,:authority)");
            query.setParameter("userid",user.getId());
            query.setParameter("authority",authority);
            query.executeUpdate();
            session.getTransaction().commit();
        }
    }

//    public void addToDrinkList(int userid, int beerid) {
//        try (Session session = sessionFactory.openSession()) {
//            Query query = session.createSQLQuery("insert into drank_beers(beer_id, user_id) " +
//                    "values (:beerid,:userid)");
//            query.setParameter("userid", userid);
//            query.setParameter("beerid", beerid);
//            query.executeUpdate();
//        }
//    }

    public void addToDrinkList(int userid, int beerid) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                User user=session.get(User.class,userid);
                Beer beer=session.get(Beer.class,beerid);
                user.getDrankBeers().add(beer);
                session.update(user);
                session.getTransaction().commit();
            }catch (HibernateException e){
                session.getTransaction().rollback();
            }

        }
    }
    public void addToWishList(int userid, int beerid) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Beer beer=session.get(Beer.class,beerid);
                User user=session.get(User.class,userid);
                user.getWantToDrinkBeers().add(beer);
                session.update(user);
                session.getTransaction().commit();
            }catch (HibernateException e){
                session.getTransaction().rollback();
            }

        }
    }



        public List<Beer> getDrankBeersList(int userid) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createNativeQuery("select b.* from beers b " +
                    "inner join drank_beers db on b.beer_id = db.beer_id " +
                    "inner join users u on db.user_id = u.user_id " +
                    "where u.user_id=:userid", Beer.class);
            query.setParameter("userid", userid);
            return query.list();
        }
    }

    public List<Beer> getWantToDrinkBeersList(int userid) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createNativeQuery("select b.* from beers b " +
                    "inner join want_to_drink wtd on b.beer_id = wtd.beer_id " +
                    "inner join users u on wtd.user_id = u.user_id " +
                    "where u.user_id=:userid", Beer.class);
            query.setParameter("userid", userid);
            return query.list();
        }
    }

    public void removeFromWishList(int userid, int beerid) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                User user = session.get(User.class, userid);
                Beer beer = session.get(Beer.class, beerid);
                user.getWantToDrinkBeers().remove(beer);
                session.update(user);
                session.getTransaction().commit();
            }catch (HibernateException e){
                session.getTransaction().rollback();
            }
        }
    }
}
