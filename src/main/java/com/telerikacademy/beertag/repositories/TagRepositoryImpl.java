package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repositories.contracts.TagRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getAllTags() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag", Tag.class);
            return query.list();
        }
    }


    @Override
    public Tag getTadById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Tag.class, id);

        }
    }

    @Override
    public Tag getTagByName(String name) {
        try (Session session=sessionFactory.openSession()){
            Query<Tag> query = session.createQuery("from Tag where name =: name",Tag.class);
            query.setParameter("name",name);
            return query.list().stream().findFirst().orElse(null);
        }
    }


    @Override
    public void createTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.saveOrUpdate(tag);
        }

    }

        @Override
    public List<Tag> getTagsByBeerId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createNativeQuery("select * from tags t " +
                    "inner join beertags b on t.tag_id = b.tag_id " +
                    "inner join beers b2 on b.beer_id = b2.beer_id " +
                    "where b2.beer_id=:id", Tag.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public Tag updateTag(Tag tag, int id) {
        try (Session session = sessionFactory.openSession()) {
            try {
                Transaction txn = session.beginTransaction();
            Query query = session.createQuery("update Tag set name=: name where tagId=:id");
            query.setParameter("name", tag.getName());
            query.setParameter("id", id);
            query.executeUpdate();
            return tag;
            }catch (Exception e) {
                session.getTransaction().rollback();
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteTag(int id) {
        try (Session session = sessionFactory.openSession()) {
            try {
                Transaction txn = session.beginTransaction();
                Query query = session.createQuery("delete Tag where tagId=:id");
                query.setParameter("id", id);
                query.executeUpdate();
            }catch (Exception e) {
                session.getTransaction().rollback();
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }


    @Override
    public void assignBeer(int tagId, int beerId) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Tag tag =session.get(Tag.class,tagId);
                Beer beer =session.get(Beer.class,beerId);
                tag.getBeers().add(beer);
                session.update(tag);
                session.getTransaction().commit();
            }catch (HibernateException e){
                session.getTransaction().rollback();
            }
        }
    }

//    public String addTagToBeer(Tag tag, int beerId) {
//        try (Session session = sessionFactory.openSession()) {
//            Beer beer = session.get(Beer.class, beerId);
//            beer.getTags().add(tag);
//            session.save(beer);
//            return tag.getName();
//        }
//    }

//    public void removeTagFromBeer(Tag tag, int beerId){
//        try (Session session = sessionFactory.openSession()){
//            Beer beer = session.get(Beer.class,beerId);
//            beer.getTags().add(tag);
//            session.remove(beer);
//        }
//    }
    @Override
    public void unAssignBeer(int tagId, int beerId) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Tag tag =session.get(Tag.class,tagId);
                Beer beer =session.get(Beer.class,beerId);
                tag.getBeers().remove(beer);
                session.update(tag);
                session.getTransaction().commit();
            }catch (HibernateException e){
                session.getTransaction().rollback();
            }
        }
    }

}
