package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repositories.contracts.BeerRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Beer getBeerById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            return beer;
        }
    }

    //name is unique?
    @Override
    public Beer getBeerByName(String name) {
        try (Session session = sessionFactory.openSession()) {
                Query<Beer> query =session.createQuery("from Beer where name =:name",Beer.class);
                query.setParameter("name",name);
                return query.list().stream().findFirst().orElse(null);
            }
    }

    @Override
    public List<Beer> getAllBeers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session
                    .createQuery("from Beer", Beer.class);
            return query.list();
        }
    }

    @Override
    public void addNewBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.save(beer);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }


    @Override
    public List<Tag> getBeerTags(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            return new ArrayList<>(beer.getTags());
        }
    }

//TODO - have to have transactions!!!!
    @Override
    public void deleteBeer(int id) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Beer beer = session.get(Beer.class, id);
                session.delete(beer);
                session.getTransaction().commit();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }

    }

    @Override
    public Beer updateBeer(int id, Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
//TODO
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
                throw e;
            }
        }
        return beer;
    }

    public List<Beer> sortByABV() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer order by alcoholByVolume", Beer.class);
            return query.list();
        }
    }

    public List<Beer> sortByAverageRating() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer order by averageRating desc", Beer.class);
            return query.list();
        }
    }

    public List<Beer> sortBeersAlphabetically() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer order by beerName", Beer.class);
            return query.list();
        }
    }

    public List<Beer> filterByTags(int tagid) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("select b from Beer as b join b.tags as t where t.tagId=:tagid");
            query.setParameter("tagid", tagid);
            return query.list();
        }
    }

    public List<Beer> filterByStyle(int styleid) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where beerStyle.id=:styleid");
            query.setParameter("styleid", styleid);
            return query.list();
        }
    }

    //TODO - fix query
    public List<Beer> filterByOriginCountry(String countryName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("select b from Beer as b join b.brewery as br join br.originCountry as c " +
                    "where c.originCountry =:countryName");
            query.setParameter("countryName", countryName);
            return query.list();
        }
    }

    public void updateAverageRating(int beerid, double averageRating){
        try (Session session = sessionFactory.openSession()){
            Query query = session.createQuery("update Beer set averageRating=:averageRating" +
                    "where id=:beerid");
            query.setParameter("averageRating",averageRating);
            query.setParameter("beerid", beerid);
            query.executeUpdate();
        }
    }

}
