package com.telerikacademy.beertag.repositories.contracts;

import com.telerikacademy.beertag.models.Country;

import java.util.List;

public interface CountryRepository {
    Country getCountryById(int id);

    List<Country> getAllCountries();

    Country getCountryByName(String name);

//    void createCountry(Country country);
//
//    Country updateCountry(int id, Country country);
//
//    void deleteCountry(int id);
}
