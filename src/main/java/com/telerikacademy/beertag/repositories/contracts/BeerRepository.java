package com.telerikacademy.beertag.repositories.contracts;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface BeerRepository {
    Beer getBeerById(int id);

    List<Beer> getAllBeers();

    Beer getBeerByName(String name);

    void addNewBeer(Beer beer);

    List<Tag> getBeerTags(int id);

    void deleteBeer(int id);

    Beer updateBeer(int id, Beer beer);

    List<Beer> sortByABV();

    List<Beer> sortByAverageRating();

    List<Beer> sortBeersAlphabetically();

    List<Beer> filterByTags(int tagid);

    List<Beer> filterByStyle(int styleid);

    List<Beer> filterByOriginCountry(String countryName);

    void updateAverageRating(int beerid, double averageRating);
}
