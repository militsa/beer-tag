package com.telerikacademy.beertag.repositories.contracts;

import com.telerikacademy.beertag.models.Brewery;

import java.util.List;

public interface BreweryRepository {
    Brewery getBreweryById(int id);

    List<Brewery> getAllBreweries();

    Brewery getByName(String name);

    void createBrewery(Brewery brewery);

    void deleteBrewery(int id);

    Brewery updateBrewery(int id, Brewery brewery);
}
