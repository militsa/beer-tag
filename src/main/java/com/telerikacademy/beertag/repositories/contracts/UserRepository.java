package com.telerikacademy.beertag.repositories.contracts;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.User;

import java.util.List;

public interface UserRepository {
    List<User> getAllUsers();

    User getUserById(int id);

    User getUserByUserName(String username);

//    User save(User user);

    void addToDrinkList(int userid, int beerid);

    void addToWishList(int userid, int beerid);

    List<Beer> getDrankBeersList(int userid);

    List<Beer> getWantToDrinkBeersList(int userid);

    void removeFromWishList(int userid, int beerid);

//    List<Beer> getTop3MostRankedBeersByUser(int userid);
void updateUserAdmin(int id, User user, String authority);
    void createUser(User user, String authority);

    void delete(int id);

    void updateUser(User user);

    User getCurrentUser();
}
