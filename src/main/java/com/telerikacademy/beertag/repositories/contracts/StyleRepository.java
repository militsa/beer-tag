package com.telerikacademy.beertag.repositories.contracts;

import com.telerikacademy.beertag.models.Style;

import java.util.List;

public interface StyleRepository {
    Style getStyleById(int id);

    Style getStyleByName(String name);

    List<Style> getAll();

    void createStyle(Style style);

    Style updateStyle(int id, Style style);

    void deleteStyle(int id);
}
