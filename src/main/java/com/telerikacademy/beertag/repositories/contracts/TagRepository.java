package com.telerikacademy.beertag.repositories.contracts;

import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getAllTags();

    Tag getTadById(int id);

    Tag getTagByName(String name);

    List<Tag> getTagsByBeerId(int id);

    void createTag(Tag tag);

    Tag updateTag(Tag tag, int id);

    void deleteTag(int id);

    //String addTagToBeer(Tag tag, int beerId);

    //void removeTagFromBeer(Tag tag, int beerId);
    public void assignBeer(int tagId, int beerId);
    public void unAssignBeer(int tagId, int beerId);
}
