package com.telerikacademy.beertag.repositories.contracts;

import com.telerikacademy.beertag.models.Rating;

import java.util.List;

public interface RatingRepository {
    List<Rating> allRatingObjects();

    void updateRating(int rating, int userId, int beerId);

    void addRatingToBeer(int rating, int userId, int beerId);

    Rating getRating(int userid, int beerid);
}
