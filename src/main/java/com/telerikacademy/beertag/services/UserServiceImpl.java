package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.exceptions.ObjectAlreadyExistsException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.repositories.contracts.BeerRepository;
import com.telerikacademy.beertag.repositories.contracts.RatingRepository;
import com.telerikacademy.beertag.repositories.contracts.TagRepository;
import com.telerikacademy.beertag.services.contracts.UserService;
import com.telerikacademy.beertag.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private RatingRepository ratingRepository;
    private BeerRepository beerRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder,  RatingRepository ratingRepository, TagRepository tagRepository, BeerRepository beerRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.ratingRepository = ratingRepository;
        this.beerRepository = beerRepository;
    }


    @Override
    public List<User> getAll() {
        return userRepository.getAllUsers();
    }

    @Override
    public User getById(int id) {
        try {
            User user = userRepository.getUserById(id);
            return user;
        } catch (Exception e) {
            throw new EntityNotFoundException(String.format("User with id %d does not exist", id));
        }
    }


    public User getUserByUserName(String username) {
        User user = userRepository.getUserByUserName(username);
        if (user == null || !user.isEnabled()) {
            throw new UsernameNotFoundException(String.format("No such user can be found"));
        }
        return user;
    }

    public void createUser(User user, String authority){
      List<User> users = userRepository.getAllUsers().stream()
              .filter(u->u.getUsername().equals(user.getUsername()))
              .collect(Collectors.toList());

      if (users.size()>0){
          throw new ObjectAlreadyExistsException(
                  String.format("User with username %s already exists",user.getUsername()));
      }

      userRepository.createUser(user, authority);
    }

    @Override
    public boolean userExists(String user) {
        User db = this.userRepository.getAllUsers().stream().filter(u -> u.getUsername().equals(user)).findAny().orElse(null);
        return db != null;
    }

    public void updateUserAdmin(int id,User user, String authority) {
        userRepository.updateUser(user);
    }

    public void update(int id,User user) {
        userRepository.updateUser(user);
    }

    public void delete(int id) {
        userRepository.delete(id);
    }

    public void addBeerToWishList(int userid, int beerid) {
        userRepository.addToWishList(userid, beerid);
    }

    public void removeBeerFromWishList(int userid, int beerid) {

        userRepository.removeFromWishList(userid, beerid);
    }

    public void addBeerToDrunkList(int userid, int beerid) {
        userRepository.addToDrinkList(userid,beerid);
    }

    public List<Beer> getDrankBeers(int userid) {
        return userRepository.getDrankBeersList(userid);
    }

    public List<Beer> getWishList(int userid) {
        return userRepository.getWantToDrinkBeersList(userid);
    }

    public User getCurrentUser(){
        return userRepository.getCurrentUser();
    }

}
