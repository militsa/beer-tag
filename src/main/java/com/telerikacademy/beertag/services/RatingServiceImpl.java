package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.exceptions.MissingOrInvalidDataException;
import com.telerikacademy.beertag.exceptions.ObjectAlreadyExistsException;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.repositories.contracts.RatingRepository;
import com.telerikacademy.beertag.services.contracts.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class RatingServiceImpl implements RatingService {

    private RatingRepository ratingRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Override
    public void update(int rating, int userid, int beerid) {
        ratingRepository.updateRating(rating, userid, beerid);
    }


    public void addRating(int rating, int userid, int beerid) {
        if (ratingRepository.getRating(userid, beerid) != null) {
            ratingRepository.updateRating(rating, userid, beerid);
        } else {
            ratingRepository.addRatingToBeer(rating, userid, beerid);
        }
    }

    @Override
    public List  <Rating> getTopTree(int userId){
        List<Rating> list=ratingRepository
                .allRatingObjects()
                .stream()
                .filter(r->r.getUser().getId()==userId)
                .sorted(Comparator.comparing(Rating::getBeerRating).reversed())
                .limit(4)
                .collect(Collectors.toList());

        if (list.size()<4){
            return list;
        }
        int x=list.get(2).getBeerRating();
        return ratingRepository
                .allRatingObjects()
                .stream()
                .filter(r->r.getBeerRating()>=x)
                .collect(Collectors.toList());
    }
}
