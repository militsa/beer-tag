package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.exceptions.MissingOrInvalidDataException;
import com.telerikacademy.beertag.exceptions.ObjectAlreadyExistsException;
import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.repositories.contracts.CountryRepository;
import com.telerikacademy.beertag.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryServiceImpl implements CountryService {

    private CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }


    @Override
    public List<Country> getAll() {
        return countryRepository.getAllCountries();
    }

    @Override
    public Country getCountryById(int id) {
        if (countryRepository.getCountryById(id)==null){
            throw new EntityNotFoundException(String.format("Country with id %d doesn't exist",id));
        }
        return countryRepository.getCountryById(id);
    }

    @Override
    public Country getCountryByName(String name) {
        if (countryRepository.getCountryByName(name)==null){
            throw new EntityNotFoundException(String.format("Country with name %s doesn't exist",name));
        }
        return countryRepository.getCountryByName(name);
    }


    // not needed methods below


//    @Override
//    public void create(Country country) {
//        List<Country> countries = countryRepository.
//           getAllCountries()
//                .stream()
//                .filter(c -> c.getOriginCountry().equals(country.getOriginCountry()))
//                .collect(Collectors.toList());
//
//        if (countries.size() > 0) {
//            throw new ObjectAlreadyExistsException(String.format("Country with name %s already exists", country.getOriginCountry()));
//        }
//        try {
//            countryRepository.createCountry(country);
//        } catch (Exception e) {
//            throw new MissingOrInvalidDataException(String.format("Missing or invalid data in any of the fields"));
//        }
//    }
//
//    @Override
//    public void update(int id, Country country) {
//        countryRepository.updateCountry(id,country);
//    }
//
//    @Override
//    public void delete(int id) {
//        countryRepository.deleteCountry(id);
//    }
}
