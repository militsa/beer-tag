package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.exceptions.ObjectAlreadyExistsException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repositories.contracts.TagRepository;
import com.telerikacademy.beertag.services.contracts.BeerService;
import com.telerikacademy.beertag.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;
    private BeerService beerService;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository,BeerService beerService) {

        this.tagRepository = tagRepository;
        this.beerService =beerService;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAllTags();
    }

    @Override
    public void create(Tag tag) {
        if (tagRepository.getTagByName(tag.getName())!=null){

            throw new ObjectAlreadyExistsException(String.format("Tag with name %s already exists",tag.getName()));
        }
        tagRepository.createTag(tag);
    }

    @Override
    public Tag update(int id, Tag tag){
        return tagRepository.updateTag(tag,id);
    }

    @Override
    public void delete(int id){
        tagRepository.deleteTag(id);
    }

    @Override
    public Tag getById(int id){
        Tag tag = tagRepository.getTadById(id);
        if (tag==null){
            throw new EntityNotFoundException(String.format("Tag with id %d not found",id));
        }
        return tagRepository.getTadById(id);
    }

    @Override
    public void assignBeer(int tagId, int beerId) {
        Tag tag =tagRepository.getTadById(tagId);
        Beer beer=beerService.getById(beerId);
        if(tag==null){
            throw new EntityNotFoundException("Not such tag id");
        }
        if(beer==null){
            throw new EntityNotFoundException("Not such beer id");
        }
        if(tag.getBeers().stream().anyMatch(t->t.getId()==beerId)){
            throw new IllegalArgumentException("already assigned");
        }
        tagRepository.assignBeer(tagId,beerId);
    }

    @Override
    public void unAssignBeer(int tagId, int beerId) {
        Tag tag =tagRepository.getTadById(tagId);
        Beer beer=beerService.getById(beerId);
        if(tag==null){
            throw new EntityNotFoundException("Not such tag id");
        }
        if(beer==null){
            throw new EntityNotFoundException("Not such beer id");
        }
        if(tag.getBeers().stream().noneMatch(t->t.getId()==beerId)){
            throw new IllegalArgumentException("not assigned");
        }
        tagRepository.unAssignBeer(tagId,beerId);
    }

    @Override
    public Tag getTagByName(String name) {
        Tag tag = tagRepository.getTagByName(name);
        if(tag==null){
            throw new EntityNotFoundException("Not such tag name");
        }
        return tag;
    }
}
