package com.telerikacademy.beertag.services.contracts;

import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface TagService {
    List<Tag> getAll();

    void create(Tag tag);

    Tag update(int id, Tag tag);

    void delete(int id);

    Tag getById(int id);

    //String addTagToBeer(Tag tag, int beerId);
    public Tag getTagByName(String name);

//    //TODO - think more over this one
//    void removeTagFromBeer(Tag tag, int beerId);

    public void assignBeer(int tagId, int beerId);
    public void unAssignBeer(int tagId, int beerId);
}
