package com.telerikacademy.beertag.services.contracts;

import com.telerikacademy.beertag.models.Brewery;

import java.util.List;

public interface BreweryService {
    List<Brewery> getAll();

    Brewery getBreweryById(int id);

    Brewery getByName(String name);

    void create(Brewery brewery);

    void update(int id, Brewery brewery);

    void delete(int id);
}
