package com.telerikacademy.beertag.services.contracts;

import com.telerikacademy.beertag.models.Country;

import java.util.List;

public interface CountryService {
    List<Country> getAll();

    Country getCountryById(int id);

    Country getCountryByName(String name);

//    void create(Country country);
//
//    void update(int id, Country country);
//
//    void delete(int id);
}
