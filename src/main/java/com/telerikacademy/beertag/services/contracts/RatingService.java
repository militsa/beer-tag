package com.telerikacademy.beertag.services.contracts;

import com.telerikacademy.beertag.models.Rating;

import java.util.ArrayList;
import java.util.List;

public interface RatingService {

    void update(int rating, int userId, int beerId);

    void addRating(int rating, int userid, int beerid);

    public List<Rating> getTopTree(int useId);
}
