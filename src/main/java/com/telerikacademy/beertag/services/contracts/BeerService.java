package com.telerikacademy.beertag.services.contracts;

import com.telerikacademy.beertag.models.Beer;

import java.util.List;

public interface BeerService {
    List<Beer> getAll();

    Beer getById(int id);

   // List<Beer> getAllSorted(String sortBy);

//    List<Beer> getBeersFilteredByCountry(String countryName);
//
//    List<Beer> getBeersFilteredByStyle(String styleName);
//
//    List<Beer> getBeersFilteredByTag(String tagName);

    void create(Beer beer);

    Beer update(int id, Beer beer);

    void delete(int id);

    public List<Beer> getList(String style, String country, String tag,String sortType);

    public Beer getBeerByName(String name);
}
