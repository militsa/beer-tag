package com.telerikacademy.beertag.services.contracts;

import com.telerikacademy.beertag.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> getAll();

    void create(Style style);

    Style update(int id, Style style);

    void delete(int id);
    Style getStyleById(int id);

    Style getStyleByName(String name);

}
