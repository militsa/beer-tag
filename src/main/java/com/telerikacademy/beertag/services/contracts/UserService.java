package com.telerikacademy.beertag.services.contracts;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.DTO.LoginUser;
import com.telerikacademy.beertag.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    User getCurrentUser();

    User getById(int id);

    User getUserByUserName(String username);

    boolean userExists(String user);

    void createUser(User user, String authority);

    void addBeerToWishList(int userid, int beerid);

    void removeBeerFromWishList(int userid, int beerid);

    void addBeerToDrunkList(int userid, int beerid);

    void updateUserAdmin(int id,User user, String authority);

    void update(int id,User user);

    void delete(int id);

//    List<Beer> showUserMostRankedBeers(int userid);

    List<Beer> getDrankBeers(int userid);

    List<Beer> getWishList(int userid);

}
