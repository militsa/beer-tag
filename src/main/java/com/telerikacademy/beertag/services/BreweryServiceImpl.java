package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.exceptions.MissingOrInvalidDataException;
import com.telerikacademy.beertag.exceptions.ObjectAlreadyExistsException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.repositories.contracts.BreweryRepository;
import com.telerikacademy.beertag.services.contracts.BreweryService;
import com.telerikacademy.beertag.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BreweryServiceImpl implements BreweryService {
    private BreweryRepository breweryRepository;
    private CountryService countryService;

    @Autowired
    public BreweryServiceImpl(BreweryRepository breweryRepository, CountryService countryService) {
        this.breweryRepository = breweryRepository;
        this.countryService = countryService;
    }

    @Override
    public List<Brewery> getAll(){
        return breweryRepository.getAllBreweries();
    }


    @Override
    public void create(Brewery brewery) {
        if (breweryRepository.getByName(brewery.getName())!=null){
            throw new ObjectAlreadyExistsException(String.format("Brewery with name %s already exists", brewery.getName()));
        }
        breweryRepository.createBrewery(brewery);
    }

    @Override
    public void update(int id, Brewery brewery){
        breweryRepository.updateBrewery(id,brewery);
    }

    @Override
    public void delete(int id){
        breweryRepository.deleteBrewery(id);
    }


    @Override
    public Brewery getBreweryById(int id) {
        Brewery brewery = breweryRepository.getBreweryById(id);
        if(brewery==null){
            throw new EntityNotFoundException(String.format("Brewery with id %d doesn't exist",id));
        }
        return brewery;
    }

    @Override
    public Brewery getByName(String name) {
        Brewery brewery = breweryRepository.getByName(name);
        if(brewery==null){
            throw new EntityNotFoundException(String.format("Brewery with name %s doesn't exist",name));
        }
        return brewery;
    }
}
