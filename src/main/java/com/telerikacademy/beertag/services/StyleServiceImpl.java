package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.exceptions.ObjectAlreadyExistsException;
import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.repositories.contracts.StyleRepository;
import com.telerikacademy.beertag.services.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StyleServiceImpl implements StyleService {
    private StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository) {
        this.styleRepository = styleRepository;
    }

    @Override
    public List<Style> getAll(){
        return styleRepository.getAll();
    }

    @Override
    public void create(Style style){
        List<Style> styles = styleRepository.getAll().stream()
                .filter(s->s.getBeerStyle().equals(style.getBeerStyle()))
                .collect(Collectors.toList());

        if (styles.size()>0){
            throw new ObjectAlreadyExistsException(String.format("Style with name %s already exists",style.getBeerStyle()));
        }
        styleRepository.createStyle(style);
    }

    @Override
    public Style update(int id, Style style){
        return styleRepository.updateStyle(id,style);
    }

    @Override
    public void delete(int id){
       styleRepository.deleteStyle(id);
    }

    @Override
    public Style getStyleById(int id) {
        if (styleRepository.getStyleById(id)==null){
            throw new EntityNotFoundException(String.format("Style with id %d doesn't exist",id));
        }
        return styleRepository.getStyleById(id);
    }

    @Override
    public Style getStyleByName(String name) {
        Style style =styleRepository.getStyleByName(name);
        if (style==null){
            throw new EntityNotFoundException(String.format("Style with name %s doesn't exist",name));
        }
        return style;
    }
}
