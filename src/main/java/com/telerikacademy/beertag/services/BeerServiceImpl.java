package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.exceptions.MissingOrInvalidDataException;
import com.telerikacademy.beertag.exceptions.ObjectAlreadyExistsException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repositories.contracts.*;
import com.telerikacademy.beertag.services.contracts.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BeerServiceImpl implements BeerService {
    private BeerRepository beerRepository;
    private BreweryRepository breweryRepository;
    private CountryRepository countryRepository;
    private StyleRepository styleRepository;
    private TagRepository tagRepository;


    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository,
                           BreweryRepository breweryRepository,
                           CountryRepository countryRepository,
                           StyleRepository styleRepository,
                           TagRepository tagRepository

    ) {
        this.beerRepository = beerRepository;
        this.breweryRepository = breweryRepository;
        this.countryRepository = countryRepository;
        this.styleRepository = styleRepository;
        this.tagRepository = tagRepository;

    }


    @Override
    public List<Beer> getAll() {
        return beerRepository.getAllBeers();
    }

    @Override
    public Beer getById(int id) {
        try {
            Beer beer = beerRepository.getBeerById(id);
            return beer;
        } catch (Exception e) {
            throw new EntityNotFoundException(String.format("Beer with id %d does not exist", id));
        }
    }

    @Override
    public void create(Beer beer) {
        List<Beer> beers = beerRepository.
                getAllBeers()
                .stream()
                .filter(b -> b.getBeerName().equals(beer.getBeerName()))
                .collect(Collectors.toList());

        if (beers.size() > 0) {
            throw new ObjectAlreadyExistsException(String.format("Beer with name %s already exists", beer.getBeerName()));
        }
        try {
            beerRepository.addNewBeer(beer);
        } catch (Exception e) {
            throw new MissingOrInvalidDataException("Missing or invalid data in any of the fields");
        }
    }

    @Override
    public Beer update(int id, Beer beer){
       return beerRepository.updateBeer(id,beer);
    }

    @Override
    public void delete(int id){
        beerRepository.deleteBeer(id);
    }

    @Override
    public List<Beer> getList(String style, String country, String tag,String sortType) {
        List<Beer> listA;
        List<Beer> listB;
        List<Beer> listC;
        List<Beer> listD;

        //filterByTag
        if (tag.equals("all")){
            listC=new ArrayList<>(beerRepository.getAllBeers());
        }else {
            listC=tagRepository.getTagByName(tag).getBeers();
        }

        //filterByStyle
        if(style.equals("all")){
            listA=new ArrayList<>(listC);
        }else {
            listA=listC.stream().filter(b -> b.getBeerStyle().getBeerStyle().equals(style)).collect(Collectors.toList());
        }

        //filterByCountry
        if (country.equals("all")){
            listB=new ArrayList<>(listA);
        }else {
            listB=listA.stream().filter(b->b.getBrewery().getOriginCountry().getOriginCountry().equals(country)).collect(Collectors.toList());
        }

        //sort the list
        if(sortType.equals("")){
            listD=new ArrayList<>(listB);
        }else if (sortType.equals("alphabetically")){
            //listD=listB.stream().sorted((b1,b2)->b1.getBeerName().compareTo(b2.getBeerName())).collect(Collectors.toList());
            listD=listB.stream().sorted(Comparator.comparing(Beer::getBeerName)).collect(Collectors.toList());
        }else if (sortType.equals("by alcohol volume")){
            listD=listB.stream().sorted(Comparator.comparing(Beer::getAlcoholByVolume).thenComparing(Beer::getAverageRating).thenComparing(Beer::getBeerName)).collect(Collectors.toList());
        }else {
            listD=listB.stream().sorted(Comparator.comparing(Beer::getAverageRating).reversed().thenComparing(Beer::getBeerName)).collect(Collectors.toList());
        }
        return listD;
    }

    @Override
    public Beer getBeerByName(String name) {
        Beer beer=beerRepository.getBeerByName(name);
        if(beer==null){
            throw new IllegalArgumentException("not such beer");
        }
        return beer;
    }
}
