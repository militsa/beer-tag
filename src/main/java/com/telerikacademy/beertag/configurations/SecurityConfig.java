package com.telerikacademy.beertag.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.sql.DataSource;

@Configuration
//@EnableWebSecurity(debug = true)
@EnableWebSecurity
@PropertySource("classpath:application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    String dbUrl, dbUsername, dbPassword, driver;

    public SecurityConfig(Environment environment) {
        dbUrl = environment.getProperty("database.url.security");
        dbUsername = environment.getProperty("database.username");
        dbPassword = environment.getProperty("database.password");
        driver = environment.getProperty("datasource.driver-class-name");
    }

    @Bean("authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public AuthenticationSuccessHandler myAuthenticationSuccessHandler() {
        return new AuthenticationSuccessHandlerImpl();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(securityDataSource())
                .authoritiesByUsernameQuery("select u.username, a.role, u.enabled from authorities a " +
                        "join users u on u.user_id = a.user_id where username = ?")
                .usersByUsernameQuery("SELECT username, password, enabled from users where username = ?");

        //        auth.inMemoryAuthentication()
//                .withUser(User.withUsername("Militsa").password("{noop}pass1").roles("USER", "ADMIN"))
//                .withUser(User.withUsername("Gosho").password("{noop}pass2").roles("USER"))
//

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/beers/new").permitAll()
//                .antMatchers("/beers/**")
//                .hasRole("USER")
                .antMatchers("/users").hasRole("ADMIN")
                .antMatchers("/createUser").hasRole("ADMIN")
                .antMatchers("/editUser/**").hasRole("ADMIN")
                .antMatchers("/deleteUser/**").hasRole("ADMIN")
                .antMatchers("/")
                .permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/authenticate")
                .successHandler(myAuthenticationSuccessHandler())
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling()
                .accessDeniedPage("/access-denied")
                .and().csrf().disable();
    }

    @Bean
    public DataSource securityDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/beers");
        dataSource.setUsername("beertag");
        dataSource.setPassword("beer");
        return dataSource;
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(securityDataSource());
        return jdbcUserDetailsManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

