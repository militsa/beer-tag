//package com.telerikacademy.beertag.controllers.Rest;
//
//import com.telerikacademy.beertag.models.Style;
//import com.telerikacademy.beertag.services.contracts.StyleService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.server.ResponseStatusException;
//
//import javax.validation.Valid;
//import java.util.List;
//
////@RestController
//@RequestMapping("/api/styles")
//public class StyleController {
//    private StyleService styleService;
//
//    @Autowired
//    public StyleController(StyleService styleService) {
//        this.styleService = styleService;
//    }
//
//    @GetMapping
//    public List<Style> getAll(){
//        return styleService.getAll();
//    }
//
//    @PostMapping("/new")
//    public Style create(@RequestBody @Valid Style style){
//        try{
//            styleService.create(style);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
//        }
//        return style;
//    }
//
//    @PutMapping("/{id}")
//    public Style update(@Valid @PathVariable int id, @Valid @RequestBody Style style){
//        try {
//            styleService.update(id,style);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//        return style;
//    }
//
//    @DeleteMapping("/{id}")
//    public void delete(@Valid @PathVariable int id){
//        try {
//            styleService.delete(id);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }
//}
