//package com.telerikacademy.beertag.controllers.Rest;
//
//import com.telerikacademy.beertag.models.Brewery;
//import com.telerikacademy.beertag.services.contracts.BreweryService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.server.ResponseStatusException;
//
//import javax.validation.Valid;
//import java.util.List;
//
////@RestController
//@RequestMapping("/api/breweries")
//public class BreweryController {
//
//    private BreweryService breweryService;
//
//    @Autowired
//    public BreweryController(BreweryService breweryService) {
//        this.breweryService = breweryService;
//    }
//
//    @GetMapping
//    public List<Brewery> getAll() {
//        return breweryService.getAll();
//    }
//
//    @PostMapping("/new")
//    public Brewery create(@Valid @RequestBody Brewery brewery) {
//        try {
//            breweryService.create(brewery);
//        } catch (RuntimeException e) {
//            throw new ResponseStatusException(
//                    HttpStatus.CONFLICT, e.getMessage());
//        }
//        return brewery;
//    }
//
//    @PutMapping("/{id}")
//    public Brewery update(@Valid @PathVariable int id, @Valid @RequestBody Brewery brewery){
//        try {
//            breweryService.update(id,brewery);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//        return brewery;
//    }
//
//    @DeleteMapping("/{id}")
//    public void delete(@Valid @PathVariable int id){
//        try {
//            breweryService.delete(id);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }
//}
