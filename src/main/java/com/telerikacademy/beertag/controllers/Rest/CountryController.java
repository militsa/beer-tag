//package com.telerikacademy.beertag.controllers.Rest;
//
//import com.telerikacademy.beertag.models.Country;
//import com.telerikacademy.beertag.services.contracts.CountryService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.server.ResponseStatusException;
//
//import javax.validation.Valid;
//import java.util.List;
//
////@RestController
//@RequestMapping("/api/countries")
//public class CountryController {
//    private CountryService countryService;
//
//    @Autowired
//    public CountryController(CountryService countryService) {
//        this.countryService = countryService;
//    }
//
//    @GetMapping
//    public List<Country> getAll(){
//        return countryService.getAll();
//    }
//
//    @PostMapping("/new")
//    public Country create(@Valid @RequestBody Country country){
//        try {
//            countryService.create(country);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
//        }
//        return country;
//    }
//
//    @PutMapping("/{id}")
//    public Country update(@PathVariable int id, @Valid @RequestBody Country country){
//        try {
//            countryService.update(id,country);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//        return country;
//    }
//
//    @DeleteMapping("/{id}")
//    public void delete(@PathVariable int id){
//        try {
//            countryService.delete(id);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }
//}
