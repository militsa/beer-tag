//package com.telerikacademy.beertag.controllers.Rest;
//
//import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
//import com.telerikacademy.beertag.models.Rating;
//import com.telerikacademy.beertag.services.contracts.RatingService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.server.ResponseStatusException;
//
//import javax.validation.Valid;
//import java.util.List;
//
////@RestController
//@RequestMapping("api/ratings")
//public class RatingController {
//    private RatingService ratingService;
//
//    @Autowired
//    public RatingController(RatingService ratingService) {
//        this.ratingService = ratingService;
//    }
//
//    @GetMapping
//    public List<Rating> getAll(){
//        return ratingService.getAll();
//    }
//
//    @GetMapping("/{id}")
//    public Rating getRatingById(@PathVariable int id){
//        try {
//            return ratingService.getRatingById(id);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }
//
//    @PostMapping("/new")
//    public Rating create(@Valid @RequestBody Rating rating){
//        try {
//            ratingService.addRating(rating);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
//        }
//        return rating;
//    }
//    @PutMapping("/{id}")
//    public Rating update(@Valid @PathVariable int id, @Valid @RequestBody Rating rating){
//        try {
//            ratingService.update(id,rating);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//        return rating;
//    }
//
//    @DeleteMapping("/{id}")
//    public void delete(@Valid @PathVariable int id){
//        try {
//            ratingService.delete(id);
//        }catch (RuntimeException e ){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }
//}
