//package com.telerikacademy.beertag.controllers.Rest;
//
//import com.telerikacademy.beertag.models.Tag;
//import com.telerikacademy.beertag.services.contracts.TagService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.server.ResponseStatusException;
//
//
//import javax.validation.Valid;
//import javax.validation.constraints.NotNull;
//import java.util.List;
//
////@RestController
//@RequestMapping("/api/tags")
//public class TagController {
//    private TagService tagService;
//
//    @Autowired
//    public TagController(TagService tagService) {
//        this.tagService = tagService;
//    }
//
//    @GetMapping
//    public List<Tag> getAll(){
//        return tagService.getAll();
//    }
//
//    @GetMapping("/{id}")
//    public Tag getTagById(@PathVariable int id){
//        try {
//            return tagService.getById(id);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }
//
//    @PostMapping("/new")
//    public Tag create(@Valid @RequestBody Tag tag){
//        try {
//            tagService.create(tag);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
//        }
//        return tag;
//    }
//
//    @PutMapping("/{id}")
//    public Tag update(@Valid @PathVariable int id, @RequestBody Tag tag){
//        try {
//            tagService.update(id,tag);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//        return tag;
//    }
//
//    @DeleteMapping("/{id}")
//    public void delete(@Valid @PathVariable int id){
//        try {
//            tagService.delete(id);
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }
//}
