//package com.telerikacademy.beertag.controllers.Rest;
//
//import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
//import com.telerikacademy.beertag.models.Beer;
//import com.telerikacademy.beertag.services.contracts.BeerService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.server.ResponseStatusException;
//
//import javax.transaction.Transactional;
//import javax.validation.Valid;
//import java.util.List;
//
////@RestController
//@RequestMapping("/api/beers")
//public class BeerController {
//    private BeerService beerService;
//
//    @Autowired
//    public BeerController(BeerService beerService) {
//        this.beerService = beerService;
//    }
//
//    @GetMapping
//    public List<Beer> getAll(@RequestParam(required = false) String sortBy,
//                             @RequestParam(required = false) String country,
//                             @RequestParam(required = false) String style,
//                             @RequestParam(required = false) String tag) {
//        if (country!=null){
//            return beerService.getBeersFilteredByCountry(country);
//        }
//        if (style!=null){
//            return beerService.getBeersFilteredByStyle(style);
//        }
//        if (tag!=null){
//            return beerService.getBeersFilteredByTag(tag);
//        }
//        if (sortBy==null){
//            return beerService.getAll();
//        }
//        return beerService.getAllSorted(sortBy);
//    }
//
//    @GetMapping("/{id}")
////    :\\d+
//    public Beer getById(@PathVariable int id) {
//        try {
//            return beerService.getById(id);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
//                    e.getMessage());
//        }
//
//    }
//
////    @GetMapping("/sorted")
////    public List<Beer> sortBy(@RequestParam(required = false) String sortByParam){
////        try {
////            return beerService.getAllSorted(sortByParam);
////        } catch (EntityNotFoundException e) {
////            throw new ResponseStatusException(
////                    HttpStatus.NOT_FOUND, e.getMessage());
////        }
////    }
//
//    @PostMapping("/new")
//    public Beer create(@Valid @RequestBody Beer beer){
//        try {
//            beerService.create(beer);
//        }catch (IllegalArgumentException e){
//            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
//        }
//        return beer;
//    }
//
//    @PutMapping("/{id}")
//    public Beer update(@Valid  @PathVariable int id, @Valid @RequestBody Beer beer){
//        try {
//            return beerService.update(id, beer);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }
//
//    @DeleteMapping("/{id}")
//    public void delete(@Valid @PathVariable int id){
//        try {
//            beerService.delete(id);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(
//                    HttpStatus.NOT_FOUND, e.getMessage());
//        }
//
//    }
//
//
//
//
//
//
//
//}
