package com.telerikacademy.beertag.controllers.Rest;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
//
//@RestController
//@RequestMapping("/api/users")
//public class UserController {
//    private UserService userService;
//
//    @Autowired
//    public UserController(UserService userService) {
//        this.userService = userService;
//    }
//
//    @GetMapping
//    public List<User> getAll(){
//        return userService.getAll();
//    }
//
//    @PostMapping("/new")
//    public User create(@Valid @RequestBody User user){
//        try {
//            userService.createUser(user,"ROLE_USER");
//        }catch (RuntimeException e){
//            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
//        }
//        return user;
//    }
//
//    @PutMapping("/{id}")
//    public User update(@Valid @PathVariable int id, @Valid @RequestBody User user){
//        try {
//            userService.update(id,user);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException((HttpStatus.NOT_FOUND), e.getMessage());
//        }
//        return user;
//    }
//
//    @DeleteMapping("/{id}")
//    public void delete(@Valid @PathVariable int id){
//        try {
//            userService.delete(id);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }
//}
