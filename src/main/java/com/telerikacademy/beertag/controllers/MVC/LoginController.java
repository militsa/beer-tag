package com.telerikacademy.beertag.controllers.MVC;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String showLogin(){
        return "login";
    }

}





















//package com.telerikacademy.beertag.controllers.MVC;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//@Controller
//public class LoginController {
//
//    @GetMapping("/login")
//    public String showLogin(){
//        return "login";
//    }
//
//    @PostMapping("/login")
//    public String postLoginPage(@RequestParam(required = false) String error, Model model){
//        if(error != null){
//            model.addAttribute("error", "Invalid credentials");
//        }
//
//        return "index";
//    }
//
//    @GetMapping("/access-denied")
//    public String showAccessDenies(){
//        return "access-denied";
//    }
//}
