package com.telerikacademy.beertag.controllers.MVC;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.models.VirtualTag;
import com.telerikacademy.beertag.repositories.VirtualTagRepo;
import com.telerikacademy.beertag.services.contracts.BeerService;
import com.telerikacademy.beertag.services.contracts.RatingService;
import com.telerikacademy.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;

@Controller
public class UserController {
    private UserService userService;
    private BeerService beerService;
    private PasswordEncoder passwordEncoder;
    private RatingService ratingService;
    private VirtualTagRepo virtualTagRepo;

    @Autowired
    public UserController(UserService userService, BeerService beerService,RatingService ratingService,VirtualTagRepo virtualTagRepo,PasswordEncoder passwordEncoder)  {
        this.userService = userService;
        this.beerService = beerService;
        this.ratingService =ratingService;
        this.virtualTagRepo=virtualTagRepo;
        this.passwordEncoder=passwordEncoder;
    }

    @GetMapping("/account")
    public String getUser(Model model) {
        User user =userService.getCurrentUser();
        model.addAttribute("user", user);
        model.addAttribute("drankList", user.getDrankBeers());
        model.addAttribute("wishList", user.getWantToDrinkBeers());
        model.addAttribute("objectToRemove",new VirtualTag());
        model.addAttribute("topTreeBeers", ratingService.getTopTree(user.getId()));
        return "myaccount";
    }

   @PostMapping("/beers/addBeerToWishlist/{id}")
    public String addBeerToWishlist(@PathVariable int id, Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        userService.addBeerToWishList(user.getId(),id);
        return "redirect:/beers/"+id;
    }


    @PostMapping("/beers/addBeerToDranklist/{id}")
    public String addBeerToDranklist(@PathVariable int id, Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        userService.addBeerToDrunkList(user.getId(),id);
        return "redirect:/beers/"+id;
    }

    @PostMapping("/beers/rateBeer/{id}")
    public String rateBeer(@PathVariable int id, @ModelAttribute VirtualTag virtualTag, Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        Beer beer=beerService.getById(id);
        virtualTagRepo.createVirtualTag(virtualTag);
        VirtualTag vt=virtualTagRepo.getVirtualTag();
        int rating=Integer.parseInt(vt.getTagType());
        if(beer.getRatings().stream().anyMatch(r->r.getUser().getId()==user.getId())){
                ratingService.update(rating,user.getId(),id);
        }
        ratingService.addRating(rating,user.getId(),id);
        return "redirect:/beers/"+id;
    }

    @PostMapping("/beers/removeFromWishList")
    public String removeBeerFromWishList(@ModelAttribute VirtualTag virtualTag) {
        User user = userService.getCurrentUser();
        virtualTagRepo.createVirtualTag(virtualTag);
        VirtualTag vt = virtualTagRepo.getVirtualTag();
        Beer beer = beerService.getBeerByName(vt.getTagType());

        userService.removeBeerFromWishList(user.getId(), beer.getId());
        return "redirect:/account";

    }

    @GetMapping("/users")
    public String showUsers(Model model) {
        model.addAttribute("users", userService.getAll());
        return "users";
    }

    @GetMapping("/createUser")
    public String createUserForm(Model model) {
        model.addAttribute("user", new User());
        return "createuser";
    }

    @PostMapping("/createUser")
    public String createUser(@ModelAttribute User user, @RequestParam String authority){
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        userService.createUser(user,authority);
        return "redirect:/users";
    }

    @GetMapping("/editUser/{id}")
    public String editUserForm(Model model, @PathVariable int id){
        model.addAttribute("user",userService.getById(id));
        return "edituser";
    }

    @PostMapping("/editUser/{id}")
    public String editUser(@PathVariable int id, @ModelAttribute User user,
                           @RequestParam String authority){
        userService.updateUserAdmin(id,user,authority);
        return "redirect:/users";
    }

    @PostMapping("/deleteUser/{id}")
    public String deleteUser(@PathVariable int id){
        userService.delete(id);
        return "redirect:/users";
    }


}





