package com.telerikacademy.beertag.controllers.MVC;


import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.services.contracts.BreweryService;
import com.telerikacademy.beertag.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class BreweryController {
    private BreweryService breweryService;
    private CountryService countryService;

    @Autowired
    public BreweryController(BreweryService breweryService, CountryService countryService) {
        this.breweryService = breweryService;
        this.countryService = countryService;
    }

    @GetMapping("/breweries/new")
    public String getNewBreweryView(Model model){
        model.addAttribute("brewery", new Brewery());
        model.addAttribute("countries",countryService.getAll());
        return "newBreweryView";
    }

    @PostMapping("/breweries/new")
    public String insertNewBrewery(@Valid @ModelAttribute Brewery brewery, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "redirect:/breweries/new";
        }

        Country country = countryService.getCountryByName(brewery.getOriginCountry().getOriginCountry());
        brewery.setOriginCountry(country);
        breweryService.create(brewery);
        return "redirect:/beers/new";
    }

    @GetMapping("/breweries/all")
    public String getAllBeersPage(Model model){
        model.addAttribute("beersList",breweryService.getAll());
        return "AllBreweries";

    }

}
