package com.telerikacademy.beertag.controllers.MVC;

import com.telerikacademy.beertag.models.*;
import com.telerikacademy.beertag.repositories.ParamsRepo;
import com.telerikacademy.beertag.repositories.VirtualTagRepo;
import com.telerikacademy.beertag.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.service.Tags;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Controller
public class BeerController {
    private BeerService beerService;
    private StyleService styleService;
    private BreweryService breweryService;
    private TagService tagService;
    private CountryService countryService;
    private ParamsRepo paramsRepo;
    private VirtualTagRepo virtualTagRepo;
    private UserService userService;

    @Autowired
    public BeerController(BeerService beerService,StyleService styleService,BreweryService breweryService,TagService tagService,CountryService countryService,ParamsRepo paramsRepo,VirtualTagRepo virtualTagRepo,UserService userService) {
        this.beerService = beerService;
        this.styleService=styleService;
        this.breweryService=breweryService;
        this.tagService=tagService;
        this.countryService=countryService;
        this.paramsRepo=paramsRepo;
        this.virtualTagRepo=virtualTagRepo;
        this.userService=userService;
    }

    @GetMapping("/beers/all")
    public String getAllBeersPage(Model model){
        model.addAttribute("beersList",beerService.getAll());
        return "AllBeers";

    }


    @GetMapping("/beers/new")
    public String getNewBeerView(Model model){
        model.addAttribute("beer",new Beer());
        model.addAttribute("styles",styleService.getAll());
        model.addAttribute("breweries",breweryService.getAll());

        return "newBeerView";
    }

    @PostMapping("/beers/new")
    public String newBeer(@RequestParam MultipartFile file, @Valid @ModelAttribute Beer beer, BindingResult bindingResult, Model model){
        model.addAttribute("file",file);
        if (bindingResult.hasErrors()){
            return "redirect:/beers/new";
        }

        Style style = styleService.getStyleByName(beer.getBeerStyle().getBeerStyle());
        Brewery breweryToSet =breweryService.getByName(beer.getBrewery().getName());
        beer.setBeerStyle(style);
        beer.setBrewery(breweryToSet);

        try {
            beer.setImage(file.getBytes());
        }catch (IOException e){
            e.printStackTrace();
        }

        beerService.create(beer);
        return "redirect:/hello";
    }

    @GetMapping("/beers/{id}")
    public String showBeerById(@PathVariable("id") int id, Model model) {
        Beer beer =beerService.getById(id);
        User user=userService.getCurrentUser();
        model.addAttribute("allTags",beer.getTags());
        model.addAttribute("beer", beer);
        model.addAttribute("wishlist",user.getWantToDrinkBeers()
                .stream()
                .filter(b->b.getId()==id)
                .collect(Collectors.toList())
        );

        model.addAttribute("drankList",user.getDrankBeers()
                .stream()
                .filter(b->b.getId()==id)
                .collect(Collectors.toList())
        );

        List<Tag> restT= new ArrayList<>();
        fillRestTagsList(beer, restT);
        model.addAttribute("restTags",restT);
        model.addAttribute("virtualTag",new VirtualTag());
        model.addAttribute("fixedRating",beerService.getById(id).getRatings()
                .stream()
                .filter(r->r.getUser().getId()==user.getId())
                .collect(Collectors.toList())
        );
        List<Integer> ratingsScale=new ArrayList<>();
        for (int i =0;i<6;i++){
            ratingsScale.add(i);
        }
        model.addAttribute("ratingScale",ratingsScale);
        return "singleBeerView";
    }


    @PostMapping("/beers/{id}")
    public String addNewTagToCurrentBeer(@PathVariable("id") int id,@ModelAttribute VirtualTag virtualTag) {
        virtualTagRepo.createVirtualTag(virtualTag);
        VirtualTag vt=virtualTagRepo.getVirtualTag();
        Tag tagToBeAdded=tagService.getTagByName(vt.getTagType());

        tagService.assignBeer(tagToBeAdded.getTagId(),id);
        return "redirect:/beers/"+id;
    }
    @PostMapping("/beers/deletedTags/{id}")
    public String removeTagFromCurrentBeer(@PathVariable("id") int id,@ModelAttribute VirtualTag virtualTag, Model model) {
        virtualTagRepo.createVirtualTag(virtualTag);
        VirtualTag vt=virtualTagRepo.getVirtualTag();
        Tag tagToBeRemoved =tagService.getTagByName(vt.getTagType());
        tagService.unAssignBeer(tagToBeRemoved.getTagId(),id);
        return "redirect:/beers/"+id;
    }

    @GetMapping("/beers/filter")
    public String filterBeers(Model model){

        List<Style> list1=styleService.getAll();
        list1.add(0,new Style("all"));
        List<Tag> list2=tagService.getAll();
        list2.add(0,new Tag("all"));
        List<Country> list3=countryService.getAll();
        list3.add(0,new Country("all"));
        List<String> list4=new ArrayList<String>();
        list4.add("");
        list4.add("alphabetically");
        list4.add("by alcohol volume");
        list4.add("by rating");
        model.addAttribute("styles", list1);
        model.addAttribute("tags", list2);
        model.addAttribute("countries", list3);
        model.addAttribute("params",new Params());
        model.addAttribute("sortList",list4);
        return "filterBeers";
    }




    @PostMapping("/beers/filter")
    public String getFilterBeers(@ModelAttribute Params params,Model model){
        paramsRepo.createParam(params);
        Params p =paramsRepo.getParams();
        //?? dali da se 4isti
        List<Beer> filterBeerList =beerService.getList(p.getStyleType(),p.getCountryType(),p.getTagType(),p.getSortType());
        model.addAttribute("beersList",filterBeerList);
        return "AllBeers";
    }




    private void fillRestTagsList(Beer beer, List<Tag> restT) {
        for (Tag tag : tagService.getAll()) {
            boolean exists = false;
            for (Tag tag1 : beer.getTags()) {
                if (tag.getTagId() == tag1.getTagId()) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                restT.add(tag);
            }
        }
    }




}
