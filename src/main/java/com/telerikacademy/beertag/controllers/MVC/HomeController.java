package com.telerikacademy.beertag.controllers.MVC;

import org.springframework.stereotype.Controller;
        import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/hello")
    public String getHomePage() {
        return "hellojoro";
    }

}
