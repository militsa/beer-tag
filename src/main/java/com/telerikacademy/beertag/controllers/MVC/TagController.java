package com.telerikacademy.beertag.controllers.MVC;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.services.contracts.BeerService;
import com.telerikacademy.beertag.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class TagController {
    private TagService tagService;
    private BeerService beerService;

    @Autowired
    public TagController(TagService tagService, BeerService beerService) {
        this.tagService = tagService;
        this.beerService = beerService;
    }

    @GetMapping("/tags/new")
    public String createNewTagView(Model model) {
        model.addAttribute("tag", new Tag());
        return "newTagView";

    }


    @PostMapping("/tags/new")
    public String insertNewTag(@Valid @ModelAttribute Tag tag, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/tags/new";
        }

        tagService.create(tag);
        return "redirect:/beers/all";

    }



}