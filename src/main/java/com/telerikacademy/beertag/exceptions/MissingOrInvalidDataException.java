package com.telerikacademy.beertag.exceptions;

public class MissingOrInvalidDataException extends RuntimeException {

    public MissingOrInvalidDataException(String message){
        super(message);
    }
}
