package com.telerikacademy.beertag.models;


import org.hibernate.annotations.Formula;

import java.util.*;

import javax.persistence.*;
import javax.transaction.Transactional;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Transactional
@Table(name = "beers")
public class Beer {
    @Id
    @PositiveOrZero
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id")
    private int id;

    @Column(name = "name", unique = true)
    @NotEmpty
    @Size(min = 1, max = 50, message = "Be more specific![(1+1=?);(50-1=?0]")
    private String beerName;

    @ManyToOne
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;

    @Lob
    @Column(name = "image", columnDefinition = "MEDIUMBLOB")
    private byte[] image;

    @Column(name = "ABV")
    private double alcoholByVolume;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style beerStyle;

    @OneToMany(mappedBy = "beer",fetch = FetchType.EAGER)
    private Set<Rating> ratings;


    @Formula("(select ifnull(avg(r.rating), 0) from beers as b join ratings as r on b.beer_id = r.beer_id " +
            "where b.beer_id = beer_id)")
    private double averageRating;

    @ManyToMany
            (fetch = FetchType.EAGER)
    @JoinTable(name = "beertags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags;


    public Beer() {
    }

    public Beer(String beerName, Brewery brewery, double alcoholByVolume, String description, Style beerStyle, double averageRating) {
        this.beerName = beerName;
        this.brewery = brewery;
        this.alcoholByVolume = alcoholByVolume;
        this.description = description;
        this.beerStyle = beerStyle;
        this.averageRating = averageRating;
        this.tags=new ArrayList<>();
        this.ratings=new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBeerName() {
        return beerName;
    }

    public void setBeerName(String beerName) {
        this.beerName = beerName;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public double getAlcoholByVolume() {
        return alcoholByVolume;
    }

    public void setAlcoholByVolume(double alcoholByVolume) {
        this.alcoholByVolume = alcoholByVolume;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Style getBeerStyle() {
        return beerStyle;
    }

    public void setBeerStyle(Style beerStyle) {
        this.beerStyle = beerStyle;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public List<Tag> getTags() {
        return tags;
    }

   // public void setTags(List<Tag> tags) {
//        this.tags = tags;
//    }


    public Set<Rating> getRatings() {
        return ratings;
    }


    public String getImage() {
        return Base64.getEncoder().encodeToString(image);
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

//    public Set<Rating> getRatings() {
//        return ratings;
//    }
//
//    public void setRatings(Set<Rating> ratings) {
//        this.ratings = ratings;
//    }
//    //

//    private static byte[] compress(byte[] in) {
//        try {
//            ByteArrayOutputStream out = new ByteArrayOutputStream();
//            DeflaterOutputStream defl = new DeflaterOutputStream(out);
//            defl.write(in);
//            defl.flush();
//            defl.close();
//
//            return out.toByteArray();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    private static byte[] decompress(byte[] in) {
//        try {
//            ByteArrayOutputStream out = new ByteArrayOutputStream();
//            InflaterOutputStream infl = new InflaterOutputStream(out);
//            infl.write(in);
//            infl.flush();
//            infl.close();
//
//            return out.toByteArray();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
}
