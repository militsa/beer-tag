package com.telerikacademy.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tags")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tag_id")
    private int tagId;

    @NotNull
    @Column(name = "name", unique = true, length = 20)
    private String name;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "beertags",
    joinColumns = @JoinColumn(name = "tag_id"),
    inverseJoinColumns = @JoinColumn(name = "beer_id"))
    @JsonIgnore
    private List<Beer> beers;

    public Tag() {
    }
    public Tag(String name) {
        this.name = name;
        this.beers=new ArrayList<>();
    }

    public int getTagId() {
        return tagId;
    }

    //public void setTagId(int tagId) {
//        this.tagId = tagId;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Beer> getBeers() {
        return beers;
    }

    public void setBeers(List<Beer> beers) {
        this.beers = beers;
    }
}
