package com.telerikacademy.beertag.models;

public class Params {
    public String styleType;
    public String countryType;
    public String tagType;
    public String sortType;

    public Params() {
    }

    public Params(String styleType, String countryType, String tagType,String sortType) {
        this.styleType = styleType;
        this.countryType = countryType;
        this.tagType = tagType;
        this.sortType=sortType;
    }


    public String getStyleType() {
        return styleType;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }

    public String getCountryType() {
        return countryType;
    }

    public void setCountryType(String countryType) {
        this.countryType = countryType;
    }

    public String getTagType() {
        return tagType;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }
}
