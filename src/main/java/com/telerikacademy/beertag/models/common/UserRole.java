package com.telerikacademy.beertag.models.common;

public enum UserRole {
    USER,
    ADMIN
}
