package com.telerikacademy.beertag.models.common;

public enum BeerStyle {
    Amber,
    Blonde,
    Brown,
    Cream,
    Dark,
    Pale,
    Strong,
    Wheat,
    Red,
    IndiaPaleAle,
    Lime,
    Pilsner,
    Golden,
    Fruit,
    Honey
}
