package com.telerikacademy.beertag.models;

public class VirtualTag {
    private String tagType;

    public VirtualTag() {
    }

    public VirtualTag(String tagType) {
        this.tagType = tagType;
    }

    public String getTagType() {
        return tagType;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }
}
