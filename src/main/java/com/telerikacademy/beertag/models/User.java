package com.telerikacademy.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.*;

@Entity
@Table(name = "users")
@Transactional
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @NotNull
    @NotEmpty
    @Email(message = "Are you sure that thi is your email address")
    @Pattern(regexp = "[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
    @Column(name = "email", unique = true)
    private String email;

    @NotNull
    @NotEmpty
    @Column(name = "username")
    private String username;

    @NotEmpty
    @NotNull
    @Column(name = "password")
    private String password;


    @Column(name = "enabled")
    private boolean enabled = true;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "drank_beers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private Set<Beer> drankBeers;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "want_to_drink",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private Set<Beer> wantToDrinkBeers;

    public User() {
    }

    public User(String username, String password, String email) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.drankBeers=new HashSet<>();
        this.wantToDrinkBeers=new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return Base64.getEncoder().encodeToString(photo);
    }

    public void setImage(byte[] image) {
        this.photo = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<Beer> getDrankBeers() {
        return drankBeers;
    }

    public void setDrankBeers(Set<Beer> drankBeers) {
        this.drankBeers = drankBeers;
    }

    public Set<Beer> getWantToDrinkBeers() {
        return wantToDrinkBeers;
    }

    public void setWantToDrinkBeers(Set<Beer> wantToDrinkBeers) {
        this.wantToDrinkBeers = wantToDrinkBeers;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

