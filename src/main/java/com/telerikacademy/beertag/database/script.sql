USE beers;

create table countries
(
    country_id int auto_increment
        primary key,
    name       varchar(20) not null
);

create table breweries
(
    brewery_id int auto_increment
        primary key,
    name       varchar(50) not null,
    country_id int         not null,
    constraint breweries_name_uindex
        unique (name),
    constraint country_id
        foreign key (country_id) references countries (country_id)
);

create table styles
(
    style_id int auto_increment
        primary key,
    style    varchar(20) not null,
    constraint styles_style_uindex
        unique (style)
);

create table beers
(
    beer_id        int auto_increment
        primary key,
    name           varchar(50)      not null,
    brewery_id     int              null,
    image          mediumblob       null,
    ABV            double           not null,
    description    mediumtext       not null,
    style_id       int              null,
    average_rating double default 0 null,
    constraint beers_image_uindex
        unique (image) using hash,
    constraint beers_name_uindex
        unique (name),
    constraint beers_styles_style_id_fk
        foreign key (style_id) references styles (style_id),
    constraint `brewery-id`
        foreign key (brewery_id) references breweries (brewery_id)
);

create table tags
(
    tag_id int auto_increment
        primary key,
    name   varchar(20) not null
);

create table beertags
(
    beer_id int not null,
    tag_id  int not null,
    constraint beer_tags
        foreign key (beer_id) references beers (beer_id),
    constraint beertags
        foreign key (tag_id) references tags (tag_id)
);

create table users
(
    user_id               int auto_increment
        primary key,
    photo                 mediumblob        null,
    email                 varchar(320)      not null,
    password              varchar(100)      not null,
    username              varchar(20)       not null,
    enabled               tinyint default 1 not null,
    password_confirmation varchar(100)      null,
    constraint users_email_uindex
        unique (email)
);

create table authorities
(
    user_id int         not null,
    role    varchar(50) not null,
    constraint user_id
        unique (user_id, role),
    constraint authorities_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table drank_beers
(
    beer_id int not null,
    user_id int not null,
    constraint drank_beers_beers_beer_id_fk
        foreign key (beer_id) references beers (beer_id),
    constraint drank_beers_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table rating
(
    id      int auto_increment
        primary key,
    rating  int not null,
    beer_id int not null,
    user_id int not null,
    constraint beer_id
        foreign key (beer_id) references beers (beer_id),
    constraint user_id
        foreign key (user_id) references users (user_id)
);

CREATE USER 'beertag'@'localhost' IDENTIFIED BY 'beer';
GRANT USAGE ON *.* TO 'beertag'@'localhost';
GRANT ALL PRIVILEGES ON `beer`.* TO 'beertag'@'localhost';
FLUSH PRIVILEGES;


