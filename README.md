![mm thirsty?](https://gitlab.com/militsa/beer-tag/raw/master/Documentation/beer.jpg)

BEER TAG enables your users to manage all the beers that they have drank and want to drink. Each beer has detailed information about it from the ABV (alcohol by volume) to the style and description. Data is community driven and every beer lover can add new beers and edit missing information on already existing ones. Also, BEER TAG allows you to rate a beer and calculates average rating from different users.


Trello Board 
https://trello.com/b/vmcPf4G4/beer-tag 